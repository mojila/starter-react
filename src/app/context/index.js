import React from "react";

export const initialState = { user: 'Hello' };

export const reducer = (state, action) => {
  switch (action.type) {
    case "change":
      return { user: action.value };
    default:
      return state;
  }
};

export const Context = React.createContext();
