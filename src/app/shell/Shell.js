import './index.scss'

import React, { useReducer } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Home from '../routes/home/Home'

// Init Context
import { Context, initialState, reducer } from '../context'

export default () => {
  const [store, dispatch] = useReducer(reducer, initialState)

  return (
    <Context.Provider value={{ store, dispatch }}>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Home}/>
        </Switch>
      </BrowserRouter>
    </Context.Provider>
  )
}