import React, { useContext } from 'react'
import { Context } from '../../context'

export default () => {
  const { store, dispatch } = useContext(Context)

  return <h2 onClick={() => dispatch({ type: "change", value: "Aji" })}>Welcome {store.user}</h2>
}
